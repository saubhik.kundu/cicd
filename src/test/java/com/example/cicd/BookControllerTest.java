package com.example.cicd;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@RunWith(SpringRunner.class)
@WebMvcTest(value = BookController.class)
public class BookControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void shouldBookControllerGetBook() throws Exception {
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
                "/api/book").accept(
                MediaType.APPLICATION_JSON_VALUE);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        //System.out.println("Result: "+result.getResponse().getContentAsString());
        String expected = "{\"book_name\":\"Two States\",\"author_name\":\"Chetan Bhagat\"}";

        Assert.assertEquals(expected, result.getResponse().getContentAsString());
    }
}
